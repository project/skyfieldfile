Sky Field File module allows you to upload files to any single drupal entity (node, user, comment and etc) without making those scary, hard to understand configurations of content types, file fields, formaters and so on!

Now you don't have to create a new file field for a node if you just want to attach a file to a single node or a user or any other single entity.

Current module is based on Sky Field module (http://drupal.org/project/skyfield), which provides even more interesting abilities!

You can upload a file and view it as:
* Link to file
* Image (you can use any image style existing on your site)
* Audio via HTML5 tag
* Video via HTML5 tag
* Or use beautiful Mediaelement player for your videos and audio (http://drupal.org/project/mediaelement)

REQUIREMENTS
------------

 * Drupal 7.x
 * Sky Field module